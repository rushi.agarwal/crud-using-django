from django.shortcuts import render
from .models import Profile
from django.views.decorators.csrf import csrf_exempt
from django.core.validators import RegexValidator
from django.http import JsonResponse
import re

@csrf_exempt
def create(request):
    if(request.method == 'POST'):
        uid = request.POST['user_id']
        user_name = request.POST['user_name']
        user_age = request.POST['user_age'] 
        user_interests = request.POST['user_interests']


        if(not(uid and user_name and user_age and user_interests)):
            return JsonResponse({'message': 'Data not found'}, status = 403)
        
        elif(not(re.match("^[a-zA-Z ]*$", user_name))):
            return JsonResponse({'message': 'Name can have only alphabets and spaces'}, status = 403)

        elif(not(re.match("^[a-zA-Z ,]*$", user_interests))):
            return JsonResponse({'message': 'Interests can have only alphabets and commas'}, status = 403)          
            
        elif(not(uid.isnumeric())):
            return JsonResponse({'message': 'Id can have only numbers'}, status = 403)

        elif(not(user_age.isnumeric()) or int(user_age) > 60):
           return JsonResponse({'message': 'Age must be less than or equal to 60'}, status = 403)

        else:
            profile = Profile.objects.all().filter(user_id = uid)
            result = bool(profile)
            if(len(profile) == 0):
                Profile.objects.create(user_id=uid, user_name=user_name, user_age=user_age, user_interests=user_interests)
                return JsonResponse({'message': 'Data inserted successfully'})
            else:
                return JsonResponse({'message': 'Data with same id already exists'}, status = 403)
    else: 
        return JsonResponse({'message': 'Data not inserted '})

@csrf_exempt
def read(request):
    if(request.method == 'POST'):

        uid = (request.POST['read_id'])

        if(not(uid)):
            return JsonResponse({'message': 'Data not found'}, status = 403)

        elif(not(uid.isnumeric())):
            return JsonResponse({'message': 'Id can have only numbers'}, status = 403)
        
        else:
            profile = Profile.objects.filter(user_id = int(uid))
            if(len(profile) == 0):
                return JsonResponse({'message': 'Record with given id not found'}, status = 403)
            else:
                dataSend = list(profile.values())
                return JsonResponse({'message': dataSend})
    else:
        return JsonResponse({'message': 'Unable to read data'}, status = 403)

@csrf_exempt
def update(request):
    if(request.method == 'POST'):

        uid = (request.POST['user_id'])
        user_field = request.POST['user_field']
        user_value = request.POST['user_value']

        
        if(not(uid and user_field and user_value)):
            return JsonResponse({'message': 'Data not found'}, status = 403)

        elif(not(uid.isnumeric())):
            return JsonResponse({'message': 'Id can have only numbers'}, status = 403)
        
        else:
            profile = Profile.objects.filter(user_id = uid)

            if(len(profile) == 0):
                return JsonResponse({'message': 'Record with given id not found'}, status = 403)
            else:
                for data in profile:
                    setattr(data, user_field, user_value)
                    data.save()
                return JsonResponse({'message': 'Record updated successfully'})
    else:
        return JsonResponse({'message': 'Unable to update record'})

@csrf_exempt
def delete(request):
    if(request.method == 'POST'):

        uid = (request.POST['user_id'])

        if(not(uid)):
            return JsonResponse({'message': 'Data not found'}, status = 403)

        elif(not(uid.isnumeric())):
            return JsonResponse({'message': 'Id can have only numbers'}, status = 403)

        else:
            profile = Profile.objects.filter(user_id = uid)
            if(len(profile) == 0):
                return JsonResponse({'message': 'Record with given id not found'}, status = 403)
            else:
                profile.delete()
                return JsonResponse({'message': 'Record deleted successfully'})
    else:
        return JsonResponse({'message': 'Unable to update record'})

from django.db import models

class Profile(models.Model):
    user_id = models.CharField(max_length=20)
    user_name = models.CharField(max_length=30)
    user_age = models.IntegerField()
    user_interests = models.TextField()



